{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Text (Text)
import Data.Aeson hiding (json)
import GHC.Generics
import Lucid
import qualified Data.Text.Lazy as L
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Web.Scotty

data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving Generic

instance ToJSON Person

mkIndex :: Html ()
mkIndex = do
    h1_ "TODO"
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "hello-society"
        body_ $ do
            h1_ "hello-scotty"
            {- old way 
            ul_ $ do
                mkLi "route1"
                mkLi "route1/route2"
                mkLi "html1"
                mkLi "bob.png"
                mkLi "json2"
            -}
            ul_ $ mapM_ mkLi ["route1", "route1/route2", "html1", "bob.png", "json2"]
    where mkLi :: Text -> Html() 
          mkLi r = li_ $ a_ [href_ r] (toHtml r)

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    middleware $ staticPolicy $ addBase "static"
    get "/bob" $ html "<img src='bob.png'>"
    get "/route1" $ text "c'est la route : route1"
    get "/route1/route2" $ text "c'est la route : route1/route2"
    get "/html1" $ html "<p>Test</p>"
    get "/json1" $ json ("toto"::String, 42::Int)
    get "/json2" $ json (Person "toto" 42)
    geft "/" $ html $ renderText mkIndex
    get "/index" $ redirect "/"
    
    get "/add1/:x/:y" $ do
        x <- param "x"
        y <- param "y"
        json (x+y :: Int)
    
    get "/add2" $ do
        -- we need to precise it's an Int for the first time, the second will be automatically int
        -- http://localhost:3000/add2?x=5&y=8 for the url 
        x <- param "x" `rescue` (\_ -> return (0::Int))
        y <- param "y" `rescue` (\_ -> return 0)
        json $ x+y