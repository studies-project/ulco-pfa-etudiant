{-# LANGUAGE OverloadedStrings #-}

module View where

import Data.Aeson hiding (json)
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Lucid

bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"


mkPage :: Html ()
mkPage = do
    h1_ "Ptivelo"
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                    , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ "ptivelo"
        body_ $ do
            h2_ "Ptivelo"
            -- div_ [class_ "container"]


