{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Web.Scotty

import Data.Maybe (fromMaybe)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Lucid

import Model
import View

main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    scotty port $ do
        middleware logStdoutDev
        middleware $ gzip def { gzipFiles = GzipCompress }
        middleware $ staticPolicy $ addBase "static"
        get "/" $ html $ renderText mkPage
        get "/riders" $ json riders