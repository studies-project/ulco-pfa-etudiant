data Tree a = Leaf | Node a (Tree a) (Tree a)

-- show left and right are recursive, show x is the default for the int
instance Show a => Show (Tree a) where
    show Leaf = "_"
    show (Node x left right) = "(" ++ show x ++ show left ++ show right ++ ")"

mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

instance Foldable Tree where
    -- foldMap :: Monoid m => (a -> m) -> t a -> m
    foldMap f Leaf = mempty
    foldMap f (Node e l r) = foldMap f l `mappend` f e `mappend` foldMap f r

main = do
    print mytree1
    -- print mytree2
    print $ sum mytree1
    print $ maximum mytree1

