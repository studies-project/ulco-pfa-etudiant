import Data.Char (toUpper)
import Data.List.Split (splitOn)

data User = User
    { _name :: String
    , _email :: String
    } deriving Show

parseUser :: String -> Maybe User
parseUser x = case splitOn ";" x of
    [name, email] -> Just (User name email)
    _ -> Nothing

upperize :: User -> User
upperize u = u { _name = map toUpper (_name u) } 

{- 
main :: IO ()
main = do
    print(parseUser "Toto;Tutu")
    line <- getLine
    -- Car Maybe foncteur donc on peut utiliser ça
    let u = upperize `fmap` parseUser line
    print u
-}

main :: IO ()
-- Si on veut être hardcore
main = fmap upperize . parseUser <$> getLine >>= print