type MyNum = Either String Double

showMyNum :: MyNum -> String
showMyNum (Left str) = "error: " ++ str
showMyNum (Right n) = "result: " ++ show n

mySqrt :: Double -> MyNum
-- mySqrt x = if x <= 0 then Left "nombre négatif" else Right (sqrt x)
mySqrt n =
    if n <= 0
        then Left "racine d'un nombre négatif"
    else Right (sqrt n)

myLog :: Double -> MyNum
-- myLog x = if x <= 0 then Left "nombre négatif" else Right (log x)
myLog n
    | n < 0 = Left "log d'un nombre négatif"
    | otherwise = Right (log n)

myMul2 :: Double -> MyNum
myMul2 x = Right (x*2)

myNeg :: Double -> MyNum
myNeg x = Right (x * (-1))

-- Calcul en cascade pour trouver des erreurs et les propager.
myCompute :: MyNum
myCompute = 
    case mySqrt(-16) of
        Left err -> Left err
        Right v -> case myNeg v of
            Left err -> Left err
            Right v2 -> myMul2 v2

{-  Simplification
myCompute :: MyNum
myCompute = do
    v <- mySqrt (-16) 
    v2 <- myNeg v 
    myMul2 v2

En fait c'est mieux celui là
main = do
print $ do
    v <- mySqrt 16
    v2 <- myNeg v 
    myMul2 v2
(modifié)
[15:16]

-}

main :: IO ()
main = do 
    print $ mySqrt (-5)
    print $ mySqrt (5)
    print $ myLog (-5)
    print $ myLog (5)
    print $ myMul2 (0)
    print $ myMul2 (5)
    print $ myNeg (-5)
    print $ myNeg (5)
    putStrLn("test")
    print (mySqrt 16 >>= myNeg >>= myMul2)

