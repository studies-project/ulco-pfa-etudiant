data User = User
    { userNom :: String
    , userPrenom :: String
    , userAge :: Int
    }

showUser :: User -> String
showUser x = userNom x ++ " " ++ userPrenom x ++ " " ++ show (userAge x)

incAge :: User -> User
incAge x = x {userAge = (userAge x) + 1 }

main :: IO ()
main = do
    -- let u = User {userNom = "Toto", userPrenom = "Titi", userAge = 25}
    let u = User "Toto" "Titi" 25
    putStrLn (showUser u)
    putStrLn(showUser (incAge u))

