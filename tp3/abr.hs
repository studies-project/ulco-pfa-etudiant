-- import Data.List (foldl')

data Tree = Leaf | Node Int Tree Tree
    deriving Show

-- insererAbr 

-- listToAbr 

-- abrToList 

main :: IO ()
main = 
    print $ Node 12
                (Node 3
                    Leaf
                    (Node 7  Leaf Leaf))
                (Node 13
                    (Node 12 Leaf Leaf)
                    (Node 42 Leaf Leaf))

