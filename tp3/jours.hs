data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche deriving Eq

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

{- We can use with a if but doesn't work because = is not defined for Jour
estWeekend j = j == Samedi || j == Dimanche
-}

-- Terminal recursion
{-
compterOuvrables :: [Jour] -> Int
compterOuvrables liste = auxCompter liste 0
    where   auxCompter [] n = n 
            auxCompter (x:xs) n =   if (estWeekend x) 
                                        then auxCompter xs (n)
                                    else
                                        auxCompter xs (n+1)
-}

compterOuvrables :: [Jour] -> Int
-- compterOuvrables jours = length (filter (\j -> not (estWeekend j)) jours)
compterOuvrables jours = length (filter (not . estWeekend) jours)

pasWeekend :: Jour -> Bool
pasWeekend = not . estWeekend


main :: IO ()
main = do
    print (estWeekend Dimanche)
    print (compterOuvrables [Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche])

