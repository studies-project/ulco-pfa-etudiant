data List a = Nil | Cons a (List a)
    deriving Show

sumList :: List Int -> Int
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs

flatList :: List String -> String
flatList Nil = ""
flatList (Cons x xs) = x ++ flatList xs

toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons x xs) = x : toHaskell xs

fromHaskell :: [a] -> List a
fromHaskell [] = Nil
fromHaskell (x:xs) = Cons x (fromHaskell xs)

myShowList :: Show a => List a -> String
myShowList Nil = ""
myShowList (Cons x xs) = show x ++ " " ++ myShowList xs

main :: IO ()
main = do
    let l1 = Nil
        l2 = Cons 20 Nil
        l3 = Cons 22 (Cons 20 Nil)
        l4 = Cons "Toto" (Cons "Titi" Nil)
    print (sumList l3)
    putStrLn (flatList l4)
    putStrLn (myShowList l3)
