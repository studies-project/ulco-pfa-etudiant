{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { first    :: T.Text
    , last     :: T.Text
    , birth    :: Int
} deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        -- <*> fmap read (v .: "birthyear")
        <*> (read <$> v .: "birthyear")

main :: IO ()
main = do
    putStrLn "\n test1"
    p1 <- eitherDecodeFileStrict' "aeson-test1.json"
    print (p1 :: Either String Person)

    putStrLn "\n test2"
    p2 <- eitherDecodeFileStrict' "aeson-test2.json"
    print (p2 :: Either String [Person])

    -- Error because we are reading an int
    putStrLn "\n test3"
    p3 <- eitherDecodeFileStrict' "aeson-test3.json"
    print (p3 :: Either String [Person])
