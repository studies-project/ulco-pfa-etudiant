import qualified Data.ByteString.Char8 as C
import qualified Data.Text.IO as T
import Data.Text.Encoding(encodeUtf8)

main :: IO()
main = do
    input <- T.readFile "text4.hs"
    C.putStrLn (encodeUtf8 input)