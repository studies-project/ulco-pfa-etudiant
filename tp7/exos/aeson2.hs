{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

{- If we don't want a generic type -}
instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"

        {-
        do
        f <- v .: "firstname"
        l <- v .: "lastname"
        return (Person f l ...)
        -}

main :: IO ()
main = do
    putStrLn "\n test1"
    p1 <- eitherDecodeFileStrict' "aeson-test1.json"
    print (p1 :: Either String Person)

    putStrLn "\n test2"
    p2 <- eitherDecodeFileStrict' "aeson-test2.json"
    print (p2 :: Either String [Person])

    putStrLn "\n test3"
    p3 <- eitherDecodeFileStrict' "aeson-test3.json"
    print (p3 :: Either String [Person])
