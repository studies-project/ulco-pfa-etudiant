import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.IO as L

main :: IO()
main = do
    input <- T.readFile "text5.hs"
    L.putStrLn (L.fromStrict input)