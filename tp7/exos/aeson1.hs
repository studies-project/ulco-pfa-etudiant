{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance FromJSON Person

main :: IO ()
main = do
    putStrLn "test1"
    p1 <- eitherDecodeFileStrict' "aeson-test1.json"
    print (p1 :: Either String Person)

    putStrLn "test2"
    p2 <- eitherDecodeFileStrict' "aeson-test2.json"
    print (p2 :: Either String [Person])

    putStrLn "test3"
    p3 <- eitherDecodeFileStrict' "aeson-test3.json"
    print (p3 :: Either String [Person])

    let res0 = Person "John" "Doe" "1970" False
    print res0

{- Other version with direct input -}
{-
import GHC.Generics
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as C
import Data.Aeson

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

test :: C.ByteString
test = "{ \"firstname\": \"John\", \"lastname\": \"Doe\", \"birthyear\": \"1970\", \"speakenglish\": false }"

main :: IO ()
main = do
    let p1 = decode test::Maybe Person
    print p1
-}