import qualified Data.ByteString.Char8 as C
import qualified Data.Text.IO as T
import Data.Text.Encoding(decodeUtf8)

main :: IO()
main = do
    input <- C.readFile "text3.hs"
    T.putStrLn (decodeUtf8 input)