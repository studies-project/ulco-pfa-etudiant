{-# LANGUAGE OverloadedStrings #-}
{- If we don't put this line "John" will be considered String and not Text -}
import qualified Data.Text as T

newtype Person = Person String deriving Show

persons :: [Person]
persons = [Person "John", Person "Haskell"]

main :: IO ()
main = print persons

