{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Database.SQLite.Simple
import Data.Text
import GHC.Generics (Generic)

data Artist = Artist
    { artist_id :: Int
    , artist_name :: Text
    } deriving (Generic, Show)

instance FromRow Artist where
    fromRow = Artist <$> field <*> field

main :: IO()
main = do
    conn <- open "music.db"
    -- We use a tuple and not Text
    res <- query_ conn "SELECT * FROM artist" :: IO [Artist]
    mapM_ print res