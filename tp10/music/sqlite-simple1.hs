{-# LANGUAGE OverloadedStrings #-}

import Database.SQLite.Simple
import Data.Text

main :: IO()
main = do
    conn <- open "music.db"
    res <- query_ conn
        "SELECT * FROM artist"
        -- We use a tuple and not Text
        :: IO [(Int, Text)]
    print res