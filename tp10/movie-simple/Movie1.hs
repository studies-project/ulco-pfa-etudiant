{-# LANGUAGE OverloadedStrings #-}

module Movie1 where

import Data.Text (Text)
import Database.SQLite.Simple

dbSelectAllMovies :: Connection -> IO [(Int, Text, Int)]
dbSelectAllMovies conn = query_ conn "SELECT * FROM movie"

dbSelectAllProds :: Connection -> IO [(Text, Int, Text, Text)]
dbSelectAllProds conn = query_ conn
    "SELECT movie_title, movie_year, person_name, role_name \
    \ FROM prod \
    \ INNER JOIN movie ON prod_movie == movie_id \
    \ INNER JOIN person ON prod_person == person_id \
    \ INNER JOIN role on prod_role == role_id"

dbSelectMoviesFromPersonId :: Connection -> Int -> IO [[Text]]
dbSelectMoviesFromPersonId conn i = query conn
    "SELECT movie_title \
    \ FROM prod \
    \ INNER JOIN movie ON prod_movie = movie_id \
    \ WHERE PROD_PERSON = ?"
    -- We don't need to put :: Only int because the function has signature with it.
    -- We use only if we have only one parameter
    (Only i)