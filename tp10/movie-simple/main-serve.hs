{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import Lucid
import Database.SQLite.Simple (open, close, withConnection)
import Web.Scotty
import Movie1
import TextShow(showt)

mkPage :: [(Text, Int, Text, Text)] -> Html ()
mkPage prods = do
    h1_ "Movies"
    {- exemple div 
    div_ $ do
        p_ "dede"
        p_ "dede"
    -}
    ul_ $ mapM_ mkLi prods

mkLi :: (Text, Int, Text, Text) -> Html ()
mkLi (movie, year, person, role) = li_ $ do
    strong_ $ toHtml movie
    " ("
    toHtml $ showt year
    "), "
    toHtml person
    " ("
    toHtml role
    ") "

main :: IO ()
main = scotty 3000 $
    get "/" $ do
        res2 <- liftIO $ withConnection "movie.db" Movie1.dbSelectAllProds
        html $ renderText $ mkPage res2
