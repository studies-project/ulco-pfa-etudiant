{-# LANGUAGE OverloadedStrings #-}

import Miso
import Miso.String (MisoString)

type Todo = (Int, MisoString)

data Model = Model
    { _todos :: [Todo]
    , _input :: MisoString
    , _nextId :: Int
    } deriving (Eq)

data Action
    = ActionNone
    | ActionAdd
    | ActionInput MisoString
    | ActionDelete Int
    deriving (Eq)

main :: IO ()
main = startApp App
    { initialAction = ActionNone
    , update        = updateModel
    , view          = viewModel
    , model         = Model [(0,"todo1"), (1,"todo2")] "" 2
    , subs          = []
    , events        = defaultEvents
    , mountPoint    = Nothing
    , logLevel      = Off
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel ActionNone m = noEff m

updateModel ActionAdd m = m' <# do
    consoleLog "hello"
    return ActionNone
    where m' = m { _todos = (_nextId m, _input m) : _todos m
                 , _input = ""
                 , _nextId = 1 + _nextId m
                 }

updateModel (ActionDelete i) m = noEff m {
    _todos = filter ((/= i) . fst) (_todos m)
    -- _todos = filter (\(ii,_) -> ii /= i) (_todos m)
}
updateModel (ActionInput str) m = noEff m { _input = str }

viewModel :: Model -> View Action
viewModel m =
    div_ []
        [ h1_ [] [ text "todo-front" ]
        , input_ [ onInput ActionInput, value_ (_input m) ]
        , button_  [onClick ActionAdd ] [text "ajouter"]
        , ul_ [] (map mkTodo (_todos m))
        ]
    where mkTodo (i, str) = li_ []
                        [ text str
                        , text " "
                        , button_ [onClick
                    (ActionDelete i)] [text "delete"]
                        ]

xhrGetUser :: IO (Maybe MisoString)
xhrGetUser = do
    let req = Request GET "api.github.com/users/juliendehos" Nothing [] False NoData
    mBstr <- contents <$> xhrByteString req
    return (mBstr >>= decodeStrict)

