{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Web.Scotty

import Control.Monad.IO.Class (liftIO)
import Data.Maybe (fromMaybe)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Database.SQLite.Simple (open, close, withConnection)
import Lucid
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import GHC.Generics
import Database.SQLite.Simple
import TextShow(showt)

dbSelectAllMessages :: Connection -> IO [(L.Text, L.Text, L.Text)]
dbSelectAllMessages conn = query_ conn
    "SELECT threadName, messagesUser, messagesContent \
    \ FROM messages \
    \ INNER JOIN thread ON messages.messagesThreadID == thread.threadID"

dbSelectAllThreads :: Connection -> IO [(Int, L.Text)]
dbSelectAllThreads conn = query_ conn
    "SELECT threadID, threadName FROM thread"

dbSelectMessFromThread :: Connection -> Int -> IO [(L.Text, L.Text)]
dbSelectMessFromThread conn i = query conn
    "SELECT messagesUser, messagesContent \
    \ FROM messages \
    \ WHERE messagesThreadID = ?"
    (Only i)

dbSelectThreadFromId :: Connection -> Int -> IO [[L.Text]]
dbSelectThreadFromId conn i = query conn
    "SELECT threadName \
    \ FROM thread \
    \ WHERE threadID = ?"
    (Only i)

bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

mkPage :: Html ()
mkPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                    , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ "ulcoforum"
        body_ $ do
            h1_ $ do
                a_ [href_ "/"] "ulcoforum"
            a_ [href_ "/alldata"] "alldata"
            " - "
            a_ [href_ "/allthreads"] "allthreads"
            hr_ []

mkIndex :: Html()
mkIndex = do
    mkPage
    p_ "This is ulcoforum"

mkAllData :: [(L.Text, L.Text, L.Text)] -> Html()
mkAllData messagesWithThreads = do
    mkPage
    p_ "All data:"
    ul_ $ mapM_ mkLiThreadsMessages messagesWithThreads

mkLiThreadsMessages :: (L.Text, L.Text, L.Text) -> Html()
mkLiThreadsMessages (messagesThreadName, messagesUser, messagesContent) = li_ $ do
    strong_ $ toHtml messagesThreadName
    br_ []
    toHtml messagesUser
    ": "
    toHtml messagesContent

mkAllThreads :: [(Int, L.Text)] -> Html()
mkAllThreads threads = do
    mkPage
    p_ "All threads:"
    ul_ $ mapM_ mkLiThreadsLinks threads

mkLiThreadsLinks :: (Int, L.Text) -> Html()
mkLiThreadsLinks (threadID, threadName) = li_ $ do
    a_ [href_ ("onethread/" <> showt threadID)] (toHtml threadName)

mkEntireThread :: L.Text -> [(L.Text, L.Text)] -> Html()
mkEntireThread threadName messagesFromThread = do
    mkPage
    p_ (toHtml threadName <> ":")
    ul_ $ mapM_ mkLiMessagesFromThreads messagesFromThread

mkLiMessagesFromThreads :: (L.Text, L.Text) -> Html()
mkLiMessagesFromThreads (messagesName, messagesContent) = li_ $ do
    toHtml messagesName
    ": "
    toHtml messagesContent


main :: IO ()
main = do
    conn <- open "app/database.db"
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    scotty port $ do
        middleware logStdoutDev
        middleware $ gzip def { gzipFiles = GzipCompress }
        middleware $ staticPolicy $ addBase "static"
        get "/" $ html $ renderText mkIndex
        get "/alldata" $ do
            res1 <- liftIO $ withConnection "app/database.db" dbSelectAllMessages
            html $ renderText $ mkAllData res1
        get "/allthreads" $ do
            res2 <- liftIO $ withConnection "app/database.db" dbSelectAllThreads
            html $ renderText $ mkAllThreads res2
        get "/onethread/:numberThread" $ do
            numberThread <- param "numberThread"
            nameThread <- liftIO $ (dbSelectThreadFromId conn numberThread)
            res3 <- liftIO $ (dbSelectMessFromThread conn numberThread)
            -- needs the !! 0 to take the first element of the list, and need to it twice
            html $ renderText $ mkEntireThread ((nameThread !! 0) !! 0) res3