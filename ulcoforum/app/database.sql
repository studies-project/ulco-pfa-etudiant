CREATE TABLE thread (
    threadID INTEGER PRIMARY KEY AUTOINCREMENT,
    threadName TEXT
);

CREATE TABLE messages (
    messagesID INTEGER PRIMARY KEY AUTOINCREMENT,
    messagesUser TEXT,
    messagesContent TEXT,
    messagesThreadID INTEGER,
    FOREIGN KEY (messagesThreadID) REFERENCES thread(threadID)
);

INSERT INTO thread VALUES (1, "Vacances 2020-2021");
INSERT INTO thread VALUES (2, "Master Info nouveau programme");

INSERT INTO messages VALUES (1, "John Doe", "Pas de vacances cette année.", 1);
INSERT INTO messages VALUES (2, "Toto", "Youpie!", 1);
INSERT INTO messages VALUES (3, "Toto", "Tous les cours passent en Haskell.", 2);