{-# LANGUAGE TypeApplications #-}

import Data.Proxy (Proxy(..))
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.Client 

import MathApi

query42 :: ClientM Int
query42 = client (Proxy @Math42)

queryMul2 :: Int -> ClientM Int
queryMul2 = client (Proxy @MathMul2)

queryAdd :: Int -> Int -> ClientM Int
queryAdd = client (Proxy @MathAdd)

query42mul2 :: Int -> ClientM (Int, Int)
query42mul2 n = do
    x <- query42
    y <- queryMul2 n
    return (x, y)

main :: IO ()
main = do
    myManager <- newManager defaultManagerSettings
    let myClient = mkClientEnv myManager (BaseUrl Http "localhost" 3000 "")

    runClientM query42 myClient >>= print
    runClientM (queryAdd 2 3) myClient >>= print
    runClientM (queryMul2 25) myClient >>= print
    runClientM (query42mul2 25) myClient >>= print