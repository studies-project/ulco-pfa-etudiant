{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy.IO as L
import Lucid

mypage :: Html()
mypage = do
    doctype_
    html_ $ do
        head_ $ meta_ [charset_ "utf8"]
        body_ $ do
            h1_ "hello"
            img_ [src_ "toto.png"]
            p_ $ do 
                "this is "
                a_ [href_ "toto.png"] "a link"

{- 
mypage :: Html()
mypage = do
    h1_ "hello"
    h2_ "hello 2a"
    h2_ "hello 2b"
    div_ $ do
        p_ "titi"
        p_ "tutu"
-}

main :: IO()
main = renderToFile "out-ludic2.html" mypage