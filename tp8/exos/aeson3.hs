{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import Data.Aeson
import qualified Data.Text as T

data Address = Address
    {   road    :: T.Text
    ,   zipcode :: Int
    ,   city    :: T.Text
    ,   number  :: Int
    } deriving (Generic)

instance ToJSON Address

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address      :: Address
    } deriving (Generic)

instance ToJSON Person

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 (Address "Pont Vieux" 43000 "Espaly" 42)
    , Person "Haskell" "Curry" 1900 (Address "Pere Lachaise" 75000 "Paris" 1337)
    ]

main :: IO()
main = encodeFile "../data/out-aeson3.json" persons

