{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import GHC.Generics
import Lucid

data Site = Site
    { url :: String
    , imgs :: [String]
    } deriving (Generic, Show)

instance FromJSON Site

type Model = [Site]

mkIndexPage :: Model -> Html ()
mkIndexPage model = do
    doctype_
    html_ $ do
        head_ $ title_ "Album"
        body_ $ do
            h1_ "Album"
            ul_ $ mapM_ (li_ . toHtml . url) model
            
main :: IO ()
main = do
    sitesE <- eitherDecodeFileStrict "data/genalbum.json"
    case sitesE of
        Left err -> putStrLn err
        Right model -> renderToFile "index.html" (mkIndexPage model)

